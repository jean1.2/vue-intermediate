const storage = {
    fetch(){
        const arr = [];        
        if(localStorage.length > 0){
            for(let i = 0; i < localStorage.length; i++){
                if(localStorage.key(i) !== 'loglevel:webpack-dev-server'){
                    arr.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
                }
            }            
        }        
        return arr;
    },
};

const state = {
    // headerText: 'TODO it!'
    todoItems : storage.fetch()
};

const getters = {
    storedTodoItems(state){
        return state.todoItems;
    }
};

const mutations = {
    addOneItem(state, todoItem){
        const obj = {completed: false, item: todoItem};
        localStorage.setItem(todoItem, JSON.stringify(obj));
        state.todoItems.push(obj);
        console.log(this.todoItems);
    },
    removeOneItem(state, payload){    
        console.log('received');                
        localStorage.removeItem(payload.todoItem.item); // remove로 객체를 지워버리면 잘 지워지지 않는다. key값에 접근해서 지워야 함
        state.todoItems.splice(payload.index, 1);
    },
    toggleOneItem(state, payload){
      // 안티패턴 : props를 내린것을 받은것을 이벤트버스로 다시 위로 올리는 방식은 좋지 않다. todoItems에 접근해서 동작하도록 하는 것이 훨씬 좋다.
      // todoItem.completed = !todoItem.completed; 
      state.todoItems[payload.index].completed = !state.todoItems[payload.index].completed; // 컴포넌트 간의 경계를 더욱 명확하게 한다.(App.vue의 컨테이너적 성격-비즈니스로직-을 더욱 살린다.)
      localStorage.removeItem(payload.todoItem.item);git
      localStorage.setItem(payload.todoItem.item, JSON.stringify(payload.todoItem));
    },
    clearAllItems(state){
        localStorage.clear();
        state.todoItems = [];
    }
}

// default와 아닌것의 차이 : default는 한개의 파일에서 딱 한개만 선언 가능
export default{
    state,
    getters,
    mutations
}