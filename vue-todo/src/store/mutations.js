const addOneItem = (state, todoItem) => {
    const obj = {completed: false, item: todoItem};
    localStorage.setItem(todoItem, JSON.stringify(obj));
    state.todoItems.push(obj);
    console.log(this.todoItems);
}
const removeOneItem = (state, payload) => {    
    console.log('received');                
    localStorage.removeItem(payload.todoItem.item); // remove로 객체를 지워버리면 잘 지워지지 않는다. key값에 접근해서 지워야 함
    state.todoItems.splice(payload.index, 1);
}
const toggleOneItem = (state, payload) => {
  // 안티패턴 : props를 내린것을 받은것을 이벤트버스로 다시 위로 올리는 방식은 좋지 않다. todoItems에 접근해서 동작하도록 하는 것이 훨씬 좋다.
  // todoItem.completed = !todoItem.completed; 
  state.todoItems[payload.index].completed = !state.todoItems[payload.index].completed; // 컴포넌트 간의 경계를 더욱 명확하게 한다.(App.vue의 컨테이너적 성격-비즈니스로직-을 더욱 살린다.)
  localStorage.removeItem(payload.todoItem.item);
  localStorage.setItem(payload.todoItem.item, JSON.stringify(payload.todoItem));
}
const clearAllItems = (state) => {
    localStorage.clear();
    state.todoItems = [];
}

export {addOneItem, removeOneItem, toggleOneItem, clearAllItems}